#include <stdio.h>
#include <time.h>

long double recursive_linear(long double x, int n) {
    if (n == 1) {
        return x;
    }
    return x * recursive_linear(x, n-1);
}

long double iterative_linear(long double x, int n) {
    long double out = 1;
    for (int i = 0; i < n; i ++) {
        out *= x;
    }
    return out;
}

long double recursive_logarithmic(long double x, int n) {
    if (n == 0) {
        return 1;
    }
    if (n & 1) {
        return x * recursive_logarithmic(x, n>>1) * recursive_logarithmic(x, n>>1);
    } else {
        return recursive_logarithmic(x, n>>1) * recursive_logarithmic(x, n>>1);
    }
}

long double iterative_logarithmic(long double x, int n) {
    long double out = 1;
    while (n > 0) {
        if (n & 1)
            out *= x;
        n = n>>1;
        x *= x;
    }
    return out;
}

int main() {
    long double x = 2;
    int n = 1000000;
    clock_t time = 0;

    printf("Calculating %Lf^%d\n", x, n);

    printf("Recursive linear:\n");
    time = clock();
    long double a1 = recursive_linear(x, n);
    time = clock() - time;
    printf("%Lf\nTime taken: %d (micros)\n\n", a1, (int) time);

    printf("Iterative linear:\n");
    time = clock();
    long double a2 = iterative_linear(x, n);
    time = clock() - time;
    printf("%Lf\nTime taken: %d (micros)\n\n", a2, (int) time);

    printf("Recursive logarithmic:\n");
    time = clock();
    long double a3 = recursive_logarithmic(x, n);
    time = clock() - time;
    printf("%Lf\nTime taken: %d (micros)\n\n", a3, (int) time);

    printf("Iterative logarithmic:\n");
    time = clock();
    long double a4 = iterative_logarithmic(x, n);
    time = clock() - time;
    printf("%Lf\nTime taken: %d (micros)\n\n", a4, (int) time);

    return 0;
}