#include <stdio.h>
#include <time.h>

unsigned long long int recursive(int n) {
    //printf("%d\n",n);
    if (n == 0) return 0;
    if (n == 1) return 1;
    return recursive(n-1) + recursive(n-2);
}

unsigned long long int iterative(int n) {
    if (n == 0) return 0;
    if (n == 1) return 1;
    unsigned long long int first = 0;
    unsigned long long int second = 1;
    unsigned long long int out = 0;

    for (int i = 2; i < n+1; i ++) {
        out = first + second;
        first = second;
        second = out;
    }
    return out;
}

int main () {
    int n = 55;
    clock_t time = 0;

    printf("Calculating Fibonacci(%d)\n\n", n);

    printf("Recursively:\n");
    time = clock();
    unsigned long long int b1 = recursive(n);
    time = clock() - time;
    printf("%llu\nTime taken: %d (micros)\n\n", b1, (int) time);

    printf("Iteratively:\n");
    time = clock();
    unsigned long long int b2 = iterative(n);
    time = clock() - time;
    printf("%llu\nTime taken: %d (micros)\n\n", b2, (int) time);

    return 0;
}